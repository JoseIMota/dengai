# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from sklearn.svm import SVR
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

train_features = pd.read_csv('DengAI_Predicting_Disease_Spread_-_Training_Data_Features.csv')
train_labels = pd.read_csv('DengAI_Predicting_Disease_Spread_-_Training_Data_Labels.csv')

predict_features = pd.read_csv('DengAI_Predicting_Disease_Spread_-_Test_Data_Features.csv')
predict_labels   = pd.read_csv('DengAI_Predicting_Disease_Spread_-_Submission_Format.csv')

#PASO1: Selección de features: Elegir con qué columnas nos quedamos
#       Eliminar columnas que no nos van a servir (ej: week_start_date)

#Rellenamos los campos con NAN mediante interpolación
train_features   = train_features.interpolate()
predict_features = predict_features.interpolate()

#Feature selection: Hay muchos campos redundantes y otros que no nos sirven
train_features = train_features.drop(columns=['week_start_date', 'station_max_temp_c', 'station_min_temp_c', 
                                      'station_avg_temp_c', 'station_precip_mm', 'station_diur_temp_rng_c', 
                                      'reanalysis_dew_point_temp_k', 'reanalysis_tdtr_k', 
                                      'reanalysis_specific_humidity_g_per_kg','reanalysis_sat_precip_amt_mm', 
                                      'reanalysis_air_temp_k', 'reanalysis_max_air_temp_k', 
                                      'reanalysis_min_air_temp_k', 'reanalysis_precip_amt_kg_per_m2'])

predict_features = predict_features.drop(columns=['week_start_date', 'station_max_temp_c', 'station_min_temp_c', 
                                      'station_avg_temp_c', 'station_precip_mm', 'station_diur_temp_rng_c', 
                                      'reanalysis_dew_point_temp_k', 'reanalysis_tdtr_k', 
                                      'reanalysis_specific_humidity_g_per_kg','reanalysis_sat_precip_amt_mm', 
                                      'reanalysis_air_temp_k', 'reanalysis_max_air_temp_k', 
                                      'reanalysis_min_air_temp_k', 'reanalysis_precip_amt_kg_per_m2'])

#Agregamos columnas para los campos con los que vamos a enriquecer los datos
#train_data = train_features.assign(total_cases = train_labels['total_cases'], summer_hum_mean = 0, summer_temp_mean = 0)
train_features   = train_features.assign(summer_hum_mean = 0, summer_temp_mean = 0, accumulated_humidity = 0, accumulated_temp = 0)
predict_features = predict_features.assign(summer_hum_mean = 0, summer_temp_mean = 0, accumulated_humidity = 0, accumulated_temp = 0)

#Enriquecemos los datos:
#       1.- Para cada ciudad y año, vamos a resumir las condiciones climáticas
#           y agregarlos como columna a cada subset de datos como corresponda,
#           ya que estudios afirman que el comienzo del verano es decisivo
#           *SAN JUAN: Verano= semanas 23 a 35
#           *IQUITOS:  Verano= semanas 48 a 9

#Veranos en San Juan: (23 a 35)
for i in range(1990,2008):
    summer_hum_mean  = train_features.query('city == "sj" and year == '+str(i)+'and weekofyear > 22 and weekofyear < 36')['reanalysis_relative_humidity_percent'].mean()
    summer_temp_mean = train_features.query('city == "sj" and year == '+str(i)+'and weekofyear > 22 and weekofyear < 36')['reanalysis_avg_temp_k'].mean()
    if i == 1990:
        aux_index = train_features.query('city == "sj" and ((year == '+str(i)+') or (year == '+str(i+1)+' and weekofyear < 36))').index.values
    else:
        aux_index = train_features.query('city == "sj" and ((year == '+str(i)+' and weekofyear > 35) or (year == '+str(i+1)+' and weekofyear < 36))').index.values
    train_features.iloc[aux_index, 10] = summer_hum_mean
    train_features.iloc[aux_index, 11] = summer_temp_mean
    
#Aplicado a los features de test (aprovechamos que los datos de test son consecutivos en fecha a los de train)
aux_index = predict_features.query('city == "sj" and year == 2008 and weekofyear < 36').index.values    
predict_features.iloc[aux_index, 10] = summer_hum_mean
predict_features.iloc[aux_index, 11] = summer_temp_mean
for i in range(2008,2013):
    summer_hum_mean  = predict_features.query('city == "sj" and year == '+str(i)+' and weekofyear > 22 and weekofyear < 36')['reanalysis_relative_humidity_percent'].mean()
    summer_temp_mean = predict_features.query('city == "sj" and year == '+str(i)+' and weekofyear > 22 and weekofyear < 36')['reanalysis_avg_temp_k'].mean()
    aux_index = predict_features.query('city == "sj" and ((year == '+str(i)+' and weekofyear > 35) or (year == '+str(i+1)+' and weekofyear < 36))').index.values
    predict_features.iloc[aux_index, 10] = summer_hum_mean
    predict_features.iloc[aux_index, 11] = summer_temp_mean

#Veranos en Iquitos: (48 a 9)
for i in range(2000,2011):
    summer_hum_mean  = train_features.query('city == "iq" and ((year == '+str(i)+' and weekofyear > 47) or (year == '+str(i+1)+' and weekofyear < 10))')['reanalysis_relative_humidity_percent'].mean()
    summer_temp_mean = train_features.query('city == "iq" and ((year == '+str(i)+' and weekofyear > 47) or (year == '+str(i+1)+' and weekofyear < 10))')['reanalysis_avg_temp_k'].mean()
    if i == 2000:
        aux_index = train_features.query('city == "iq" and ((year == '+str(i)+') or (year == '+str(i+1)+') or(year == '+str(i+2)+' and weekofyear < 10))').index.values
    else:
        aux_index = train_features.query('city == "iq" and ((year == '+str(i+1)+' and weekofyear > 9) or (year == '+str(i+2)+' and weekofyear < 10))').index.values
    train_features.iloc[aux_index, 10] = summer_hum_mean
    train_features.iloc[aux_index, 11] = summer_temp_mean
    
#Aplicado a los features de test (aprovechamos que los datos de test son consecutivos en fecha a los de train)
aux_index = predict_features.query('city == "iq" and (year == 2010 or ( year == 2011 and weekofyear < 10))').index.values    
predict_features.iloc[aux_index, 10] = summer_hum_mean
predict_features.iloc[aux_index, 11] = summer_temp_mean
for i in range(2010,2013):
    summer_hum_mean  = predict_features.query('city == "iq" and ((year == '+str(i)+' and weekofyear > 47) or (year == '+str(i+1)+' and weekofyear < 10))')['reanalysis_relative_humidity_percent'].mean()
    summer_temp_mean = predict_features.query('city == "iq" and ((year == '+str(i)+' and weekofyear > 47) or (year == '+str(i+1)+' and weekofyear < 10))')['reanalysis_avg_temp_k'].mean()
    aux_index = predict_features.query('city == "iq" and ((year == '+str(i+1)+' and weekofyear > 9) or (year == '+str(i+2)+' and weekofyear < 10))').index.values
    predict_features.iloc[aux_index, 10] = summer_hum_mean
    predict_features.iloc[aux_index, 11] = summer_temp_mean


#%%
#       2.- Para cada ciudad y semana, vamos a agregar columnas de hum/temp
#           de 3 semanas anteriores, puesto que es el tiempo que el mosquito
#           tarda en madurar: La cantidad de casos que haya dependerá en parte
#           de cómo de buenas fueron las condiciones para que el mosquito se críe
# OJO!!! Estas columnas extra es necesario agregarlas también a predict_features

#Dividimos el dataset por ciudad 
train_feat_iq = train_features.loc[train_features['city'] == 'iq'].drop(columns=['city']).reset_index(drop=True)
train_feat_sj = train_features.loc[train_features['city'] == 'sj'].drop(columns=['city']).reset_index(drop=True)

#Dividimos también los casos de predicción por ciudad
predict_feat_iq = predict_features[predict_features['city'] == 'iq'].drop(columns=['city']).reset_index(drop=True)
predict_feat_sj = predict_features[predict_features['city'] == 'sj'].drop(columns=['city']).reset_index(drop=True)


#Train San Juan
train_feat_sj.loc[0, 'accumulated_humidity'] = train_feat_sj.loc[0, 'reanalysis_relative_humidity_percent'] * 3
train_feat_sj.loc[0, 'accumulated_temp']     = train_feat_sj.loc[0, 'reanalysis_avg_temp_k'] * 3
train_feat_sj.loc[1, 'accumulated_humidity'] = train_feat_sj.loc[1, 'reanalysis_relative_humidity_percent'] * 2 +\
                                               train_feat_sj.loc[0, 'reanalysis_relative_humidity_percent']
train_feat_sj.loc[1, 'accumulated_temp']     = train_feat_sj.loc[1, 'reanalysis_avg_temp_k'] * 2 +\
                                               train_feat_sj.loc[0, 'reanalysis_avg_temp_k']                                                
train_feat_sj.loc[2, 'accumulated_humidity'] = train_feat_sj.loc[2, 'reanalysis_relative_humidity_percent'] + \
                                               train_feat_sj.loc[1, 'reanalysis_relative_humidity_percent'] + \
                                               train_feat_sj.loc[0, 'reanalysis_relative_humidity_percent']
train_feat_sj.loc[2, 'accumulated_temp']     = train_feat_sj.loc[2, 'reanalysis_avg_temp_k'] + \
                                               train_feat_sj.loc[1, 'reanalysis_avg_temp_k'] + \
                                               train_feat_sj.loc[0, 'reanalysis_avg_temp_k']

for i in range(3, len(train_feat_sj)):
    train_feat_sj.loc[i, 'accumulated_humidity'] = train_feat_sj.loc[i-1, 'reanalysis_relative_humidity_percent'] + \
                                                   train_feat_sj.loc[i-2, 'reanalysis_relative_humidity_percent'] + \
                                                   train_feat_sj.loc[i-3, 'reanalysis_relative_humidity_percent']
    train_feat_sj.loc[i, 'accumulated_temp']     = train_feat_sj.loc[i-1, 'reanalysis_avg_temp_k'] + \
                                                   train_feat_sj.loc[i-2, 'reanalysis_avg_temp_k'] + \
                                                   train_feat_sj.loc[i-3, 'reanalysis_avg_temp_k']

#Train Iquitos
train_feat_iq.loc[0, 'accumulated_humidity'] = train_feat_iq.loc[0, 'reanalysis_relative_humidity_percent'] * 3
train_feat_iq.loc[0, 'accumulated_temp']     = train_feat_iq.loc[0, 'reanalysis_avg_temp_k'] * 3
train_feat_iq.loc[1, 'accumulated_humidity'] = train_feat_iq.loc[1, 'reanalysis_relative_humidity_percent'] * 2 +\
                                               train_feat_iq.loc[0, 'reanalysis_relative_humidity_percent']
train_feat_iq.loc[1, 'accumulated_temp']     = train_feat_iq.loc[1, 'reanalysis_avg_temp_k'] * 2 +\
                                               train_feat_iq.loc[0, 'reanalysis_avg_temp_k']                                                
train_feat_iq.loc[2, 'accumulated_humidity'] = train_feat_iq.loc[2, 'reanalysis_relative_humidity_percent'] + \
                                               train_feat_iq.loc[1, 'reanalysis_relative_humidity_percent'] + \
                                               train_feat_iq.loc[0, 'reanalysis_relative_humidity_percent']
train_feat_iq.loc[2, 'accumulated_temp']     = train_feat_iq.loc[2, 'reanalysis_avg_temp_k'] + \
                                               train_feat_iq.loc[1, 'reanalysis_avg_temp_k'] + \
                                               train_feat_iq.loc[0, 'reanalysis_avg_temp_k']

for i in range(3, len(train_feat_iq)):
    train_feat_iq.loc[i, 'accumulated_humidity'] = train_feat_iq.loc[i-1, 'reanalysis_relative_humidity_percent'] + \
                                                   train_feat_iq.loc[i-2, 'reanalysis_relative_humidity_percent'] + \
                                                   train_feat_iq.loc[i-3, 'reanalysis_relative_humidity_percent']
    train_feat_iq.loc[i, 'accumulated_temp']     = train_feat_iq.loc[i-1, 'reanalysis_avg_temp_k'] + \
                                                   train_feat_iq.loc[i-2, 'reanalysis_avg_temp_k'] + \
                                                   train_feat_iq.loc[i-3, 'reanalysis_avg_temp_k']

#Predict San Juan
predict_feat_sj.loc[0, 'accumulated_humidity'] = predict_feat_sj.loc[0, 'reanalysis_relative_humidity_percent'] * 3
predict_feat_sj.loc[0, 'accumulated_temp']     = predict_feat_sj.loc[0, 'reanalysis_avg_temp_k'] * 3
predict_feat_sj.loc[1, 'accumulated_humidity'] = predict_feat_sj.loc[1, 'reanalysis_relative_humidity_percent'] * 2 +\
                                                 predict_feat_sj.loc[0, 'reanalysis_relative_humidity_percent']
predict_feat_sj.loc[1, 'accumulated_temp']     = predict_feat_sj.loc[1, 'reanalysis_avg_temp_k'] * 2 +\
                                                 predict_feat_sj.loc[0, 'reanalysis_avg_temp_k']                                                
predict_feat_sj.loc[2, 'accumulated_humidity'] = predict_feat_sj.loc[2, 'reanalysis_relative_humidity_percent'] + \
                                                 predict_feat_sj.loc[1, 'reanalysis_relative_humidity_percent'] + \
                                                 predict_feat_sj.loc[0, 'reanalysis_relative_humidity_percent']
predict_feat_sj.loc[2, 'accumulated_temp']     = predict_feat_sj.loc[2, 'reanalysis_avg_temp_k'] + \
                                                 predict_feat_sj.loc[1, 'reanalysis_avg_temp_k'] + \
                                                 predict_feat_sj.loc[0, 'reanalysis_avg_temp_k']

for i in range(3, len(predict_feat_sj)):
    predict_feat_sj.loc[i, 'accumulated_humidity'] = predict_feat_sj.loc[i-1, 'reanalysis_relative_humidity_percent'] + \
                                                     predict_feat_sj.loc[i-2, 'reanalysis_relative_humidity_percent'] + \
                                                     predict_feat_sj.loc[i-3, 'reanalysis_relative_humidity_percent']
    predict_feat_sj.loc[i, 'accumulated_temp']     = predict_feat_sj.loc[i-1, 'reanalysis_avg_temp_k'] + \
                                                     predict_feat_sj.loc[i-2, 'reanalysis_avg_temp_k'] + \
                                                     predict_feat_sj.loc[i-3, 'reanalysis_avg_temp_k']

#Predict Iquitos
predict_feat_iq.loc[0, 'accumulated_humidity'] = predict_feat_iq.loc[0, 'reanalysis_relative_humidity_percent'] * 3
predict_feat_iq.loc[0, 'accumulated_temp']     = predict_feat_iq.loc[0, 'reanalysis_avg_temp_k'] * 3
predict_feat_iq.loc[1, 'accumulated_humidity'] = predict_feat_iq.loc[1, 'reanalysis_relative_humidity_percent'] * 2 +\
                                                 predict_feat_iq.loc[0, 'reanalysis_relative_humidity_percent']
predict_feat_iq.loc[1, 'accumulated_temp']     = predict_feat_iq.loc[1, 'reanalysis_avg_temp_k'] * 2 +\
                                                 predict_feat_iq.loc[0, 'reanalysis_avg_temp_k']                                                
predict_feat_iq.loc[2, 'accumulated_humidity'] = predict_feat_iq.loc[2, 'reanalysis_relative_humidity_percent'] + \
                                                 predict_feat_iq.loc[1, 'reanalysis_relative_humidity_percent'] + \
                                                 predict_feat_iq.loc[0, 'reanalysis_relative_humidity_percent']
predict_feat_iq.loc[2, 'accumulated_temp']     = predict_feat_iq.loc[2, 'reanalysis_avg_temp_k'] + \
                                                 predict_feat_iq.loc[1, 'reanalysis_avg_temp_k'] + \
                                                 predict_feat_iq.loc[0, 'reanalysis_avg_temp_k']

for i in range(3, len(predict_feat_iq)):
    predict_feat_iq.loc[i, 'accumulated_humidity'] = predict_feat_iq.loc[i-1, 'reanalysis_relative_humidity_percent'] + \
                                                     predict_feat_iq.loc[i-2, 'reanalysis_relative_humidity_percent'] + \
                                                     predict_feat_iq.loc[i-3, 'reanalysis_relative_humidity_percent']
    predict_feat_iq.loc[i, 'accumulated_temp']     = predict_feat_iq.loc[i-1, 'reanalysis_avg_temp_k'] + \
                                                     predict_feat_iq.loc[i-2, 'reanalysis_avg_temp_k'] + \
                                                     predict_feat_iq.loc[i-3, 'reanalysis_avg_temp_k']
        
#%%
#Nos quedamos sólo con la columna que nos interesa para entrenar, dividido por ciudad
train_lab_iq = train_labels.loc[train_labels['city'] == 'iq']['total_cases'].reset_index(drop=True)
train_lab_sj = train_labels.loc[train_labels['city'] == 'sj']['total_cases'].reset_index(drop=True)

#       Tratar los datos:
#       Hay que tener en cuenta que tratamos con 2 ciudades totalmente diferentes
#       Es mejor realizar 2 regresiones, una por cada ciudad, hacemos la regresión 
#       y luego unimos los vectores de resultado para darles el formato requerido
#       NO OLVIDARSE DE REDONDEAR!!! NO PUEDEN DARSE 5.3 CASOS!!!

#Realizamos las 2 regresiones y predecimos los valores 
#(ESTE CASO ES SOLO ILUSTRATIVO, ESTAMOS PREDICIENDO SOBRE LOS PROPIOS VALORES DE TRAINING)
svr_rbf1 = SVR(kernel='rbf', epsilon=0.1,  C=500)
svr_rbf2 = SVR(kernel='rbf', epsilon=0.1,  C=500)
X_train, X_test, y_train, y_test = train_test_split(train_feat_iq, train_lab_iq, test_size=0.4, random_state=0)
prediction_iq = svr_rbf1.fit(X_train, y_train).predict(X_test)
print(svr_rbf1.score(X_test, y_test))

# Imprimirmos los resultados
lw = 2
X_test = X_test.reset_index(drop=True)
y_test = y_test.reset_index(drop=True)
plt.scatter(X_test.index, y_test, color='darkorange', label='data')
plt.plot(prediction_iq, color='navy', lw=lw, label='iq')
plt.xlabel('data')
plt.ylabel('target')
plt.title('Support Vector Regression')
plt.legend()
plt.show()

X_train, X_test, y_train, y_test = train_test_split(train_feat_sj, train_lab_sj, test_size=0.4, random_state=0)
prediction_sj = svr_rbf2.fit(X_train, y_train).predict(X_test)
print(svr_rbf2.score(X_test, y_test))

# Imprimirmos los resultados
X_test = X_test.reset_index(drop=True)
y_test = y_test.reset_index(drop=True)
plt.scatter(X_test.index, y_test, color='red', label='data')
plt.plot(prediction_sj, color='green', lw=lw, label='sj')
plt.xlabel('data')
plt.ylabel('target')
plt.title('Support Vector Regression')
plt.legend()
plt.show()

svr_rbf_iq = SVR(kernel='rbf', epsilon=0.1,  C=500)
svr_rbf_sj = SVR(kernel='rbf', epsilon=0.1,  C=500)

prediction_iq = svr_rbf_iq.fit(train_feat_iq, train_lab_iq).predict(predict_feat_iq)
prediction_sj = svr_rbf_sj.fit(train_feat_sj, train_lab_sj).predict(predict_feat_sj)
#Unimos los vectores y los agregamos al formato de entrega
prediction = np.append(prediction_sj, prediction_iq)
predict_labels['total_cases'] = prediction.round(decimals=0).astype(int)
#Guardamos los datos en formato CSV
predict_labels.to_csv('out_attempt6_sin3sem.csv', index=False)

#Imprimirmos los resultados
lw = 2
plt.plot(prediction_iq, color='navy', lw=lw, label='iq')
plt.xlabel('data')
plt.ylabel('target')
plt.title('Support Vector Regression')
plt.legend()
plt.show()

plt.plot(prediction_sj, color='green', lw=lw, label='sj')
plt.xlabel('data')
plt.ylabel('target')
plt.title('Support Vector Regression')
plt.legend()
plt.show()
